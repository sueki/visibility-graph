#include"PlanarGeometry.h"

ostream& operator<<(ostream& os, const Vertex& v){
	os << "( " << v.x << " , " << v.y << " )";
	return os;
}

ostream& operator<<(ostream& os, const Line& line){
	os << "[ " << line.from << " , " << line.to << " ]";
	return os;
}

bool PlanarGeometry::intersect_line_segment(Line a, Line b){
	double tb1 = (a.from.x - a.to.x)*(b.from.y - a.from.y) + (a.from.y - a.to.y)*(a.from.x - b.from.x);
	double tb2 = (a.from.x - a.to.x)*(b.to.y - a.from.y) + (a.from.y - a.to.y)*(a.from.x - b.to.x);
	return tb1*tb2 < 0;
}

bool PlanarGeometry::intersect(Line a, Line b){
	bool res = intersect_line_segment(a, b) && intersect_line_segment(b, a);
	//cout << (res ? "yes" : "no")<<endl;
	return res;
}


double PlanarGeometry::outer_product(Line a, Line b){
	return (a.to.x - a.from.x)*(b.to.y - b.from.y) - (a.to.y - a.from.y)*(b.to.x - b.from.x);
}

double PlanarGeometry::inner_product(Line a, Line b){
	return (a.to.x - a.from.x)*(b.to.x - b.from.x) + (a.to.y - a.from.y)*(b.to.y - b.from.y);
}

double PlanarGeometry::distance(Vertex a, Vertex b){
	return hypot(a.x - b.x, a.y - b.y);
}

void PlanarGeometry::draw_line(cv::Mat &dst, Vertex a, Vertex b, int thickness, cv::Scalar color){
	cv::line(dst, cv::Point(a.x, a.y), cv::Point(b.x, b.y), color, thickness);
}

double PlanarGeometry::length(polygon &p){
	double res = 0;
	for (int i = 0; i < p.size()-1; i++){
		res += distance(p[i], p[i + 1]);
	}
	res += distance(p[0], p[p.size() - 1]);
	return res;
}

Vertex PlanarGeometry::get_point(polygon &p, double d){
	double total_length = length(p);
	if (d >= total_length){
		int rounds = (int)(floor(d / total_length));
		d = d - rounds*total_length;
	}
	for (int i = 0; i < p.size() - 1; i++){
		double distance_now = distance(p[i], p[i + 1]);
		if (d>distance_now)d -= distance_now;
		else{
			double ratio = d / distance_now;
			return inner_partition(p[i], p[i + 1], ratio);
		}
	}
	double distance_now = distance(p[0], p[p.size() - 1]);
	return inner_partition(p[p.size() - 1], p[0], d/distance_now);
}

//内分。ratio0がaで、ratio1がb。
Vertex PlanarGeometry::inner_partition(Vertex a, Vertex b, double ratio){
	return Vertex(a.x*(1.0 - ratio) + b.x*ratio, a.y*(1.0 - ratio) + b.y*ratio);
}

bool PlanarGeometry::on_the_line(Line a, Vertex x){
	Vertex next(a.to.x, a.to.y);
	Vertex now(a.from.x, a.from.y);
	double f = (x.y - now.y)*(next.x - now.x) - (next.y - now.y)*(x.x - now.x);

	if (abs(f) <= 1e-6){
		if (next.x - now.x != 0){
			double ratio = (x.x - now.x) / (next.x - now.x);
			return ratio >= 0 && ratio <= 1;
		}
		else{
			double ratio = (x.y - now.y) / (next.y - now.y);
			return ratio >= 0 && ratio <= 1+1e-6;
		}
	}
	return false;
}