#ifndef __VISIBILITY_GRAPH__
#define __VISIBILITY_GRAPH__
#include<vector>
#include<queue>
#include <functional>
#include<opencv2/opencv.hpp>
#include"PlanarGeometry.h"
using namespace std;


/*
VisibilityGraph
頂点IDは
0 : start
1 : goal
2~2+n : obstacle
*/
class VisibilityGraph{
private:
	vector<polygon> obstacles;						// 頂点座標
	vector<vector<double > > edges;		// エッジのコスト、負の大きい数だとエッジ無
	Vertex start=Vertex(0,0,0), goal=Vertex(800,800,1);								// 始点と終点

	//エッジの行き先とコスト
	vector<pair<int, double>> edge_list[3000];

	//UIへの表示用
	cv::Mat polygon_only, obstacle_graph_only, result;

	//ポリゴンの時計回りで次の頂点
	Vertex next_vertex(const int obstacle_id, const int vertex_id);
	//ポリゴンの時計回りで前の頂点
	Vertex prev_vertex(const int obstacle_id, const int vertex_id);

	//1d ID -> (polygon ID, vertex ID)
	vector<pair<int, int> > vertex_map;

	// 内部を通るエッジじゃないかの確認
	bool intersect_inner(int obstacle_id, Vertex from, int vertex_id);

	// O(n^3)の全探索な可視性判定
	bool visible_naive(Vertex from, Vertex to);

	//障害物だけ描く。dstを初期化する。
	void draw_polygon(cv::Mat &dst);

	//障害物から成るエッジのみ（start, goalを含まない）を描く。dstは初期化されない。
	void draw_obstacle_graph(cv::Mat &dst, bool initialize = false);


public:
	cv::Size windowSize = cv::Size(800, 800);

	// constructor
	VisibilityGraph(vector<polygon> &polygons);	
	
	// とりあえず障害物だけでedges作る
	void create();

	// start, goalからのエッジ作る
	void create_variable_edge();

	// startからgoalまでの最短距離を求める。showすると表示する。
	double shortestPath(bool show=true);

	// start,goalを再設定する。start,goalは変更される前提なので初期化時には代入しない
	void set_start_and_goal(Vertex _start, Vertex _goal);
	void set_goal(Vertex v);
	void set_start(Vertex v);

	//グラフを見せる。とりあえず障害物だけ描こう。
	void show();

	//画面上に最短経路以外のエッジを表示するかを示す変数。ゴミゴミ。
	int show_edge = 0;

	//1d ID -> (polygon ID, vertex ID)
	Vertex get_vertex(pair<int, int> ids);

	//始点、終点を端点に持つエッジのみを描く。dstは初期化されない。
	void draw_variable_graph(cv::Mat &dst, bool initialize = false);

	void show_result();
	void init_result(bool graph = false);

	bool on_the_line(int obstacle_id, int vertex_id, Vertex from);
};

#endif