#include<iostream>

#include"VisibilityGraph.h"
#include"PolygonFromImage.h"
#include"thin.h"

using namespace std;

void negate_main(cv::Mat &src){
	for (int y = 0; y < src.rows; y++){
		for (int x = 0; x < src.cols; x++){
			src.at<uchar>(y, x) = (src.at<uchar>(y, x) >= 125 ? 0 : 255);
		}
	}
}

bool reachable(VisibilityGraph &g, Vertex start, Vertex goal, double limit){
	g.set_start_and_goal(start, goal);
	double time = g.shortestPath();
	return time <= limit;
}

void solve(polygon p, Vertex start, int vl, int vp){
	VisibilityGraph g(vector<polygon>(1,p));
	g.create();
	g.init_result(true);
	Vertex goal = g.get_vertex(make_pair(0, 0));

	g.set_start_and_goal(start, goal);
	g.create_variable_edge();
	double initial_time = g.shortestPath();
	double round_time = PlanarGeometry::length(p) / vl;
	int rounds = (int)(floor(initial_time / round_time));
	double single_time = initial_time - round_time*rounds;
/*
	double tlen = PlanarGeometry::length(p);
	for (double x = 0.0; x < tlen; x += 1.0){
		Vertex current_goal = PlanarGeometry::get_point(p, x);
		g.set_goal(current_goal);
		g.shortestPath(true);
		g.show_result();
	}
*/
	pair<int, double> l, r;
	l = make_pair(rounds - 1, 0), r = make_pair(rounds-1, single_time);
	cout << round_time<<" "<<initial_time<<" "<<single_time << endl;
	double delta = 0.001;
	Vertex current_goal = goal;
	while (l.second < r.second - delta){
		pair<int, double> m(rounds - 1, (l.second + r.second) / 2.0);
		double time_limit = m.first*single_time; m.second;
		Vertex current_goal = PlanarGeometry::get_point(p, m.second*vl);
		cout << m.second*vl << endl;
		cout << current_goal << endl;
		if (reachable(g, start, current_goal, time_limit)){
			l = m;
		}
		else{
			r = m;
		}
		g.show();
	}
}

int main(){
	string imgPath;
	cin >> imgPath;
	if (imgPath == "solve"){
		//solve ICPC problem
		int n=1;
		while (n != 0){
			cin >> n;
			cout << n << endl;
			int x, y;
			polygon p;
			for (int i = 0; i < n; i++){
				cin >> x >> y;
				p.push_back(Vertex(x+100, -y+400));
			}
			int px, py;
			cin >> px >> py;
			int vl, vp;
			cin >> vl >> vp;

			solve(p, Vertex(px+100, -py+400), vl, vp);
		}

		cv::Mat src = cv::imread("ahiru.tif", 0);
		cv::imshow("src", src);
		cv::waitKey();
		return 0;
	}
	if (imgPath == "image"){
		cin >> imgPath;
		cv::Mat src = cv::imread(imgPath, 0);
		cv::Mat dst, dst2 = src.clone(), dst3 = src.clone(), dst4=src.clone();
		PolygonFromImage::extract_boundary(src, dst);
		negate_main(dst);
		thin::thinning(dst, dst2);
		cv::imshow("src", src);
		cv::imshow("dst2", dst2);

		vector<deque<cv::Point> > hoge;
		vector<deque<cv::Point> > hoge2;
		PolygonFromImage::traverse_pixel(dst2, hoge);
		PolygonFromImage::count_polygon(hoge);
		//thin::extractLine(dst2, hoge);
		hoge2=thin::vectorizeAll(hoge, 2.5);
		PolygonFromImage::count_polygon(hoge2);
		thin::showLine(hoge2, dst3);
		PolygonFromImage::show_polygon(hoge2, dst4);

		cv::imshow("dst3", dst3);
		cv::imshow("dst4", dst4);
		cv::waitKey();
		double scale;
		string output_filename;
		cin >> scale >> output_filename;
		PolygonFromImage::output_polygon(output_filename, hoge2, scale);
		return 0;
	}
	cin >> imgPath;
	vector<polygon> ps;
	polygon p;
	p.push_back(Vertex(150, 200));
	p.push_back(Vertex(300, 200));
	p.push_back(Vertex(350, 600));
	p.push_back(Vertex(100, 450));
	ps.push_back(p);
	p.clear();
	p.push_back(Vertex(450, 200));
	p.push_back(Vertex(600, 200));
	p.push_back(Vertex(800, 600));
	p.push_back(Vertex(400, 450));
	ps.push_back(p);
	//VisibilityGraph v=VisibilityGraph(ps);
	vector<polygon> polygons_;
	PolygonFromImage::input_polygon(imgPath, polygons_);
	VisibilityGraph v=VisibilityGraph(polygons_);
	v.create();
	v.show();
	
	return 0;
}