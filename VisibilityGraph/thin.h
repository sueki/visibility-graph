#ifndef __THIN__
#define __THIN__

#include<stack>
#include<deque>
#include<fstream>
#include <opencv2/opencv.hpp>
using namespace std;

namespace thin{
	void thinning(cv::Mat &src, cv::Mat &dst);
	void extractLine(cv::Mat &src, vector<deque<cv::Point> > &v);
	void showLine(vector<deque<cv::Point> > &v, cv::Mat &dst);
	deque<cv::Point> vectorize(deque<cv::Point> &v, double error_lim=1.5);
	vector<deque<cv::Point> > vectorizeAll(vector<deque<cv::Point> > &v, double error_lim=1.5);
	double distance(cv::Point a, cv::Point b, cv::Point c);
	void outputLines(vector<vector<cv::Point> > &lines, string ofpath);

	//画像からエッジ抽出、methodは1がDoG
	void extractEdge(cv::Mat &src, cv::Mat &dst, int method = 1);

	//画像から１フレーム分の線を抽出
	vector<vector<cv::Point> > lineFromFrame(cv::Mat &src);

	//(線の集合のvector)から、1フレームごとのエッジのpolylineをofpathに書き出す
	void outputMovie(vector<vector<vector<cv::Point> > > &lines, string ofpath);
}

#endif