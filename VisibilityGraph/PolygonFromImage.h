#ifndef __POLYGON_FROM_IMAGE__
#define __POLYGON_FROM_IMAGE__

#include<iostream>
#include<fstream>
#include<opencv2/opencv.hpp>
#include"PlanarGeometry.h"

using namespace std;

namespace PolygonFromImage{
	void extract_boundary(cv::Mat &src, cv::Mat &dst);
	void show_polygon(vector<deque<cv::Point>> &points, cv::Mat &dst);

	//ポリゴン数、頂点数を報告する。
	void count_polygon(vector<deque<cv::Point>> &points);

	void traverse_pixel(cv::Mat &src, vector<deque<cv::Point>> &points);

	//filenameにポリゴンたちを出力する。
	void output_polygon(const string filename, const vector<deque<cv::Point> > &points, double scale=1.0);

	void input_polygon(const string filename, vector<polygon> &points);
}

#endif