#include"PolygonFromImage.h"

int dx[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
int dy[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };

void PolygonFromImage::extract_boundary(cv::Mat &src, cv::Mat &dst){
	int dx[4] = { 0, 0, 1, -1 };
	int dy[4] = { -1, 1, 0, 0 };
	//int dx[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	//int dy[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };

	dst = cv::Mat::zeros(src.size(), CV_8U);
	for (int y = 0; y < src.rows; y++){
		for (int x = 0; x < src.cols; x++){
			if (src.at<uchar>(y, x)>250){
				for (int i = 0; i < 4; i++){
					int nx = x + dx[i], ny = y + dy[i];
					if (min(nx, ny) >= 0 && nx < src.cols && ny < src.rows){
						if (src.at<uchar>(ny, nx) <10)dst.at<uchar>(y, x) = 255;
					}
				}
			}
		}
	}
}

void PolygonFromImage::show_polygon(vector<deque<cv::Point>> &points, cv::Mat &dst){


	for (int i = 0; i < points.size(); i++){
		vector<cv::Point> points1;
		for (int j = 0; j < points[i].size(); j++){
			points1.push_back(points[i][j]);
		}
		vector<vector<cv::Point> > points2;
		points2.push_back(points1);
		cv::fillPoly(dst, points2, cv::Scalar(127));
	}
}

int count_black_neighbor(cv::Mat &src, int y, int x){
	int res = 0;
	for (int i = 0; i < 8; i++){
		int nx = x + dx[i], ny = y + dy[i];
		if (min(ny, nx)>=0 && nx<src.cols && ny<src.rows && src.at<uchar>(ny, nx) == 0){
			res++;
		}
	}
	return res;
}

deque<cv::Point> traverse_loop(cv::Mat &src, int y, int x){
	deque<cv::Point> res;
	res.push_back(cv::Point(x, y));
	src.at<uchar>(y, x) = 255;

	cv::Point next(-1, -1), prev;
	for (int i = 0; i < 8; i++){
		int nx = x + dx[i], ny = y + dy[i];
		if (min(ny, nx) >= 0 && nx<src.cols && ny<src.rows && src.at<uchar>(ny, nx) == 0){
			next = cv::Point(nx, ny);
			break;
		}
	}

	while (next.x >= 0){
		res.push_back(next);
		prev = next;
		src.at<uchar>(prev) = 255;
		int max_neighbors = -1;
		for (int i = 0; i < 8; i++){
			int nx = prev.x + dx[i], ny = prev.y + dy[i];
			if (min(ny, nx) >= 0 && nx < src.cols && ny < src.rows && src.at<uchar>(ny, nx) == 0){
				int neighbors = count_black_neighbor(src, ny, nx);
				if (max_neighbors <= neighbors){
					src.at<uchar>(next) = 255;
					next = cv::Point(nx, ny);
					max_neighbors = neighbors;
				}
				else if (max_neighbors>neighbors){
					src.at<uchar>(ny, nx) = 255;
				}
			}
		}
		if (max_neighbors < 0)break;
	}

	return res;
}

void PolygonFromImage::traverse_pixel(cv::Mat &src, vector<deque<cv::Point>> &points){
	int dx[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int dy[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };
	points.clear();

	cv::Mat tmp = src.clone();
	for (int y = 0; y < tmp.rows; y++){
		for (int x = 0; x < tmp.cols; x++){
			if (tmp.at<uchar>(y, x) == 0){
				points.push_back(traverse_loop(tmp, y, x));
			}
		}
	}
}

void PolygonFromImage::count_polygon(vector<deque<cv::Point>> &points){
	cout << "PolygonFromImage::count_polygon ..." << endl;
	cout << "number of polygons : " << points.size() << endl;
	int num_vertices = 0;
	for (int i = 0; i < points.size(); i++)num_vertices += points[i].size();
	cout << "number of vertices : " << num_vertices << endl;
}

void PolygonFromImage::output_polygon(const string filename, const vector<deque<cv::Point> > &points, double scale){
	cerr << "PolygonFromImage::output_polygon ... ";
	ofstream ofs(filename);
	ofs << points.size() << endl;
	for (int i = 0; i < points.size(); i++){
		ofs << points[i].size() << endl;
		for (int j = 0; j < points[i].size(); j++){
			ofs << points[i][j].x*scale << " " << points[i][j].y*scale << endl;
		}
	}
	ofs.close();
	cerr << "complete." << endl;
}

void PolygonFromImage::input_polygon(const string filename, vector<polygon> &points){
	ifstream ifs(filename);
	points.clear();

	int num_polygons;
	ifs >> num_polygons;
	for (int i = 0; i < num_polygons; i++){
		int num_vertices;
		ifs >> num_vertices;
		polygon obstacle;
		for (int j = 0; j < num_vertices; j++){
			double x, y;
			ifs >> x >> y;
			if (j < num_vertices-1)obstacle.push_back(Vertex(x, y));
		}
		if (obstacle.size()>=3)points.push_back(obstacle);
	}
}