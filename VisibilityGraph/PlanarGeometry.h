#ifndef __PLANAR_GEOMETRY__
#define __PLANAR_GEOMETRY__

#include<iostream>
#include<vector>
#include<opencv2/opencv.hpp>
using namespace std;

class Vertex{
public:
	double x;
	double y;
	int id;
	Vertex(){};
	Vertex(double _x, double _y) : x(_x), y(_y) {};
	Vertex(double _x, double _y, int _id) : x(_x), y(_y), id(_id) {};
	friend ostream& operator<<(ostream& os, const Vertex& v);
};

typedef vector<Vertex> polygon;

class Line{
public:
	Vertex from, to;
	Line(Vertex _from, Vertex _to) : from(_from), to(_to) {};

	friend ostream& operator<<(ostream& os, const Line& dt);
};

namespace PlanarGeometry{
	// 直線と線分の交差判定
	bool intersect_line_segment(Line a, Line b);

	// 線分と線分の交差判定
	bool intersect(Line a, Line b);

	// 直線（というかベクトル）の外積をとる(a.x*b.y-a.y*b.x)
	double outer_product(Line a, Line b);

	double inner_product(Line a, Line b);		//ほげほげふがふが

	//頂点の距離
	double distance(Vertex a, Vertex b);

	void draw_line(cv::Mat &dst, Vertex a, Vertex b, int thickness, cv::Scalar color);

	//polygonの総距離
	double length(polygon &p);

	//polygonの始点からx離れたところの頂点座標
	Vertex get_point(polygon &p, double d);

	//内分。ratio0がaで、ratio1がb。
	Vertex inner_partition(Vertex a, Vertex b, double ratio);

	bool on_the_line(Line a, Vertex x);
}

#endif