#include "VisibilityGraph.h"

VisibilityGraph::VisibilityGraph(vector<polygon> &polygons){
	this->obstacles = polygons;
	//start, goal
	vertex_map.push_back(make_pair(-1, 0));
	vertex_map.push_back(make_pair(-1, 1));
	for (int i = 0; i < obstacles.size(); i++){
		for (int j = 0; j < obstacles[i].size(); j++){
			obstacles[i][j].id = vertex_map.size();
			vertex_map.push_back(make_pair(i, j));
		}
	}
}

void VisibilityGraph::set_start_and_goal(Vertex _start, Vertex _goal){
	set_start(_start);
	set_goal(_goal);
}

void VisibilityGraph::set_start(Vertex v){
	start = v;
	start.id = 0;
}

void VisibilityGraph::set_goal(Vertex v){
	goal = v;
	goal.id = 1;
}


Vertex VisibilityGraph::get_vertex(pair<int, int> ids){
	if (ids.first >= 0)return obstacles[ids.first][ids.second];
	else return (ids.second == 0 ? start : goal);
}

bool VisibilityGraph::visible_naive(Vertex from, Vertex to){
	pair<int, int> from_id = vertex_map[from.id];
	pair<int, int> to_id = vertex_map[to.id];
	if (from_id.first == to_id.first && from_id.first >= 0 && (abs(from_id.second - to_id.second) == 1 || abs(from_id.second - to_id.second) == obstacles[from_id.first].size()-1))return true;
	if (to_id.first >= 0 && intersect_inner(to_id.first, from, to_id.second))return false;
	if (from_id.first >= 0 && intersect_inner(from_id.first, to, from_id.second))return false;
	for (int i = 0; i < obstacles.size(); i++){
		for (int j = 0; j < obstacles[i].size() - 1; j++){
			if (PlanarGeometry::intersect(Line(from, to), Line(obstacles[i][j], obstacles[i][j + 1])))return false;
		}
		if (PlanarGeometry::intersect(Line(from, to), Line(obstacles[i][0], obstacles[i][obstacles[i].size() - 1])))return false;
	}
	return true;
}

static bool dragl = false;
static bool dragr = false;
void onMouse_visGraph(int event, int x, int y, int flag, void* pt){
	VisibilityGraph *g = (VisibilityGraph *)pt;
	switch (event){
	case cv::EVENT_LBUTTONDOWN:
		dragl = true;
		break;
	case cv::EVENT_LBUTTONUP:
		dragl = false;
		g->shortestPath(true);
		break;
	case cv::EVENT_RBUTTONDOWN:
		dragr = true;
		break;
	case cv::EVENT_RBUTTONUP:
		dragr = false;
		g->shortestPath(true);
		break;
	case CV_EVENT_MOUSEMOVE:
		if (dragl||dragr){
			if (min(x, y) < 0 || x >= g->windowSize.width || y >= g->windowSize.height)return;
			if (dragl)g->set_start(Vertex(x, y));
			else g->set_goal(Vertex(x, y));
		}
	}
}

void VisibilityGraph::show(){
	result = obstacle_graph_only.clone();
	draw_variable_graph(result);
	cv::imshow("result", result);
	cv::createTrackbar("edge", "result", &show_edge, 1);
	cv::setMouseCallback("result", onMouse_visGraph, (void *)(this));
	while (1){
		if (dragl||dragr){
			draw_variable_graph(result, true);
		}
		cv::imshow("result", result);
		int key = cv::waitKey(30);
		if (key == 27)break;
	}
}


Vertex VisibilityGraph::next_vertex(const int obstacle_id, const int vertex_id){
	if (vertex_id < obstacles[obstacle_id].size() - 1)return obstacles[obstacle_id][vertex_id + 1];
	else return obstacles[obstacle_id][0];
}
Vertex VisibilityGraph::prev_vertex(const int obstacle_id, const int vertex_id){
	if (vertex_id >0)return obstacles[obstacle_id][vertex_id - 1];
	else return obstacles[obstacle_id][obstacles[obstacle_id].size() - 1];
}

bool VisibilityGraph::on_the_line(int obstacle_id, int vertex_id, Vertex from){
	//cout << obstacles.size() << " " << obstacle_id << " " << obstacles[obstacle_id].size() << " " << vertex_id << endl;
	Vertex next = next_vertex(obstacle_id, vertex_id);
	Vertex now = obstacles[obstacle_id][vertex_id];
	return PlanarGeometry::on_the_line(Line(now, next), from);
}

bool VisibilityGraph::intersect_inner(int obstacle_id, Vertex from, int vertex_id){
	Line l12(from, obstacles[obstacle_id][vertex_id]);
	Line l2_prev(prev_vertex(obstacle_id, vertex_id), obstacles[obstacle_id][vertex_id]);
	Line l2_next(obstacles[obstacle_id][vertex_id], next_vertex(obstacle_id, vertex_id));
	double sign_prev = PlanarGeometry::outer_product(l12, l2_prev), sign_next = PlanarGeometry::outer_product(l12, l2_next), sign_pn = PlanarGeometry::outer_product(l2_prev, l2_next);

	int next_vertex_id = (vertex_id == obstacles[obstacle_id].size() - 1 ? 0 : vertex_id + 1);
	int prev_vertex_id = (vertex_id == 0 ? obstacles[obstacle_id].size() - 1 : vertex_id - 1);

	if (on_the_line(obstacle_id, vertex_id, from) || on_the_line(obstacle_id, prev_vertex_id, from)){
		return false;
	}

	
	if (sign_next == 0){
		if (PlanarGeometry::inner_product(l12, l2_next) < 0){
			return intersect_inner(obstacle_id, from, next_vertex_id);
		}
	}

	if (sign_prev < 0){
		if (sign_next < 0 || sign_pn >= 0)return true;
	}
	else if (sign_prev>0){
		if (sign_pn>0 && sign_next < 0)return true;
	}
	else{
		if (PlanarGeometry::inner_product(l12, l2_prev) < 0){
			if(sign_next==0)return intersect_inner(obstacle_id, from, (vertex_id == obstacles[obstacle_id].size() - 1 ? 0 : vertex_id + 1));
			else if (sign_next < 0)return true;
		}
		else{
			return intersect_inner(obstacle_id, from, (vertex_id == 0 ? obstacles[obstacle_id].size() - 1 : vertex_id - 1));
		}
	}
	//if (sign_prev == 0 && PlanarGeometry::inner_product(l12, l2_prev) < 0 && sign_next < 0) return true;
	//if ((sign_prev < 0 && (sign_next<0 || sign_pn >= 0)) || (sign_prev>0 && sign_pn>0 && sign_next < 0)) return true;
	return false;
}

void VisibilityGraph::create(){
	//initialization
	int num_vertices = vertex_map.size();
	edges = vector<vector<double > >(num_vertices, vector<double>(num_vertices, -100));

	for (int i = 2; i < num_vertices; i++){
		Vertex from = get_vertex(vertex_map[i]);
		for (int j = i + 1; j < num_vertices; j++){
			Vertex to = get_vertex(vertex_map[j]);
			if (visible_naive(from, to))edges[i][j] = edges[j][i] = PlanarGeometry::distance(from, to);
		}
	}

	draw_polygon(polygon_only);
	obstacle_graph_only = polygon_only.clone();
	draw_obstacle_graph(obstacle_graph_only);
}

void VisibilityGraph::create_variable_edge(){
	int num_vertices = vertex_map.size();
	edges[1][0] = edges[0][1] = (visible_naive(start, goal) ? PlanarGeometry::distance(start, goal) : -1000000000000);
	for (int j = 2; j < num_vertices; j++){
		Vertex to = get_vertex(vertex_map[j]);
		if (visible_naive(start, to)){
			edges[0][j] = edges[j][0] = PlanarGeometry::distance(start, to);
		}
		edges[0][j] = edges[j][0] = (visible_naive(start, to) ? PlanarGeometry::distance(start, to) : -1000000000000);
		edges[1][j] = edges[j][1] = (visible_naive(goal, to) ? PlanarGeometry::distance(goal, to) : -1000000000000);
	}
}

void VisibilityGraph::draw_polygon(cv::Mat &dst){
	dst = cv::Mat::zeros(windowSize, CV_8U);
	for (int y = 0; y < dst.rows; y++){
		for (int x = 0; x < dst.cols; x++){
			dst.at<uchar>(y, x) = 255;
		}
	}
	for (int i = 0; i < obstacles.size(); i++){
		vector<cv::Point> points;
		for (int j = 0; j < obstacles[i].size(); j++){
			points.push_back(cv::Point(obstacles[i][j].x, obstacles[i][j].y));
			//cv::circle(dst, points[j], 3, cv::Scalar(0), 1);
		}

		vector<vector<cv::Point> > points2;
		points2.push_back(points);
		cv::fillPoly(dst, points2, cv::Scalar(127));
	}
}

void VisibilityGraph::draw_obstacle_graph(cv::Mat &dst, bool initialize){
	if (initialize){
		dst = cv::Mat::zeros(windowSize, CV_8U);
		for (int y = 0; y < dst.rows; y++){
			for (int x = 0; x < dst.cols; x++){
				dst.at<uchar>(y, x) = 255;
			}
		}
	}

	int num_vertices = vertex_map.size();
	for (int i = 2; i < num_vertices; i++){
		for (int j = i + 1; j < num_vertices; j++){
			if (edges[i][j]>0){
				Vertex from = get_vertex(vertex_map[i]);
				Vertex to = get_vertex(vertex_map[j]);
				cv::line(dst, cv::Point(from.x, from.y), cv::Point(to.x, to.y), cv::Scalar(0));
			}
		}
	}
}

void VisibilityGraph::draw_variable_graph(cv::Mat &dst, bool initialize){
	create_variable_edge();
	if (initialize){
		if(show_edge)dst = obstacle_graph_only.clone();
		else dst = polygon_only.clone();
	}

	int num_vertices = vertex_map.size();
	for (int i = 0; i < 2; i++){
		for (int j = i + 1; j < num_vertices; j++){
			if (edges[i][j]>0){
				Vertex from = get_vertex(vertex_map[i]);
				Vertex to = get_vertex(vertex_map[j]);
				cv::line(dst, cv::Point(from.x, from.y), cv::Point(to.x, to.y), cv::Scalar(0));
			}
		}
	}
}

double VisibilityGraph::shortestPath(bool show){

	int num_vertices = vertex_map.size();

	//initialize edge list
	for (int i = 0; i < num_vertices; i++)edge_list[i].clear();
	for (int i = 0; i < num_vertices; i++){
		for (int j = i + 1; j < num_vertices; j++){
			if (edges[i][j] >= 0){
				edge_list[i].push_back(make_pair(j, edges[i][j]));
				edge_list[j].push_back(make_pair(i, edges[i][j]));
			}
		}
	}

	for (int i = 0; i < num_vertices; i++){
		for (int j = 0; j < edge_list[i].size(); j++){
			//cout << " "<<edge_list[i][j].first << " " << edge_list[i][j].second << endl;
		}
	}

	//run dijkstra
	vector<double> cost(num_vertices, 10000000000000.0), prev(num_vertices, -1);
	cost[0] = 0;
	priority_queue<pair<double, int>, vector<pair<double, int> >, std::greater<pair<double, int> > > q;
	q.push(make_pair(0.0, 0));

	while (!q.empty()){
		pair<double, int> now = q.top();
		q.pop();
		int from = now.second;
		for (int i = 0; i < edge_list[from].size(); i++){
			int to = edge_list[from][i].first;
			double new_cost = now.first + edge_list[from][i].second;
			if (new_cost < cost[to]){
				cost[to] = new_cost;
				prev[to] = from;
				q.push(make_pair(new_cost, to));
			}
		}
	}

	if (show){
		int idx = 1;
		while (prev[idx] >= 0){
			PlanarGeometry::draw_line(result, get_vertex(vertex_map[idx]), get_vertex(vertex_map[prev[idx]]), 3, cv::Scalar(0));
			idx = prev[idx];
			cv::circle(result, cv::Point(start.x, start.y), 5, cv::Scalar(128));
			cv::circle(result, cv::Point(goal.x, goal.y), 5, cv::Scalar(50));
		}

	}

	return cost[1];
}

void VisibilityGraph::show_result(){
	draw_variable_graph(result);
	cv::circle(result, cv::Point(start.x, start.y), 3, cv::Scalar(128));
	cv::circle(result, cv::Point(goal.x, goal.y), 3, cv::Scalar(128));
	cv::imshow("result", result);
	cv::waitKey();
	result = obstacle_graph_only.clone();
	/*
		for (int i = 2; i < vertex_map.size(); i++){
		if (edges[0][i]>=0)cout << i << " ";
		}
		puts("");
		for (int i = 2; i < vertex_map.size(); i++){
		if (edges[1][i]>=0)cout << i << " ";
		}
		puts("");
		*/
}

void VisibilityGraph::init_result(bool graph){
	if (graph)result = obstacle_graph_only.clone();
	else result = polygon_only.clone();
}